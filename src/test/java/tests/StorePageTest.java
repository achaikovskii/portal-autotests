package tests;

import jdk.jfr.Description;
import org.junit.Test;
import pages.StorePage;

public class StorePageTest extends AbstractTest {
    StorePage storePage;

    @Description("Проверка добавлления товара в корзину и его удаления")
    @Test
    public void testAddAndDeleteItemToBasket(){
        storePage = new StorePage(driver);
        storePage.goToStore();
        Integer priceChosenItem = storePage.chooseItemCheckPrice();
        storePage.addFirstItemToBasket();
        assert (storePage.getSummaryPrice().equals(priceChosenItem));
        assert (storePage.deleteOneItemFromBasket().equals("Корзина пуста"));
    }

    @Description("Проверка перехода по страницам магазина")
    @Test
    public void testNavigateStore(){
        storePage = new StorePage(driver);
        storePage.goToStore();
        assert (storePage.checkNameItemOnPage("Ужин с Екатериной Зуевой"));
        assert (storePage.getNumberThisPage().equals(1));
        storePage.goToNumberPage("4");
        assert (storePage.checkNameItemOnPage("Держатель кольца"));
        assert (storePage.getNumberThisPage().equals(4));
        storePage.goToPointerPage("›");
        assert (storePage.checkNameItemOnPage("Рюкзак CHELSY"));
        assert (storePage.getNumberThisPage().equals(5));
        storePage.goToNumberPage(" 13 ");
        assert (storePage.checkNameItemOnPage("Ужин с Сергеем Хотимским"));
        assert (storePage.getNumberThisPage().equals(13));
        storePage.goToPointerPage("‹");
        assert (storePage.checkNameItemOnPage("Ужин с Александром Дворским"));
        assert (storePage.getNumberThisPage().equals(12));
    }

    @Description("Проверка имени товаров в списке и в карточках")
    @Test
    public void testCompareItemNamesInListWithCards(){
        storePage = new StorePage(driver);
        storePage.goToStore();
        storePage.compareNameItem();
    }
}
