package tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.AuthorizationPage;
import pages.Credentials;
import java.util.concurrent.TimeUnit;

public class AbstractTest {
    AuthorizationPage authorizationPage;
    public static WebDriver driver;

    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://portal.sovcombank.ru/");
    }

    public static void closeBrowser(){
        driver.close();
        driver.quit();
        driver = null;
    }


    @Before
    public void setUp() {
        openBrowser();
        authorizationPage = new AuthorizationPage(driver);
        authorizationPage.inputAuthorizationForm(Credentials.getLogin(), Credentials.getPassword());
        authorizationPage.closeModalWindow();
    }

    @After
    public void clearAll(){closeBrowser();}

}
