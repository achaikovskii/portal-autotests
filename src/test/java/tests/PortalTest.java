package tests;

import jdk.jfr.Description;
import org.junit.Test;
import pages.PortalPage;

public class PortalTest extends AbstractTest{
    PortalPage portalPage;

    @Description("Проверка меню и основных блоков портала")
    @Test
    public void testGeneralBlocksOfPortal(){
        portalPage = new PortalPage(driver);
        portalPage.checkMenu();
        portalPage.getContentOfPortal();
    }

    @Description("Проверка перехода по главным новостям портала")
    @Test
    public void testOpenFavoriteNews(){
        portalPage = new PortalPage(driver);
        portalPage.checkTitleOfFavoriteNews();
    }

    @Description("Проверка открытия новостей в футере портала")
    @Test
    public void testOpenNews(){
        portalPage = new PortalPage(driver);
        portalPage.checkTitleOfNews();
    }
}
