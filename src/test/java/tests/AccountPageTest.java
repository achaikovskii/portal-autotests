package tests;

import jdk.jfr.Description;
import org.junit.*;
import pages.*;


public class AccountPageTest extends AbstractTest {
    AccountPage mainPageOfPortal;
    String nickname = "Старорусов Станислав Игоревич";

    @Description("Проверка личного кабинета")
    @Test
    public void testAccountData(){
        mainPageOfPortal = new AccountPage(driver);
        assert (mainPageOfPortal.getTitleOfPersonalAccount().contains("Александр"));
        mainPageOfPortal.goIntoPersonalAccount();
        assert (mainPageOfPortal.getUserNameFromPersonAccount().contains("Александр"));
        assert (mainPageOfPortal.getUserNameFromPersonAccount().contains("Чайковский"));
        assert (mainPageOfPortal.getEmailText()).equals("chaikovskiias@sovcombank.ru");
        assert (mainPageOfPortal.getPhoneText()).equals("+79297778047");
    }

    @Description("Проверка функционала кармы в личном кабинете")
    @Test
    public void testGiveCarma(){
        mainPageOfPortal = new AccountPage(driver);
        mainPageOfPortal.goIntoPersonalAccount();
        mainPageOfPortal.checkGiveAwayCarma();
    }

    @Description("Проверка поисковой строки")
    @Test
    public void testFindUser(){
        mainPageOfPortal = new AccountPage(driver);
        authorizationPage.findUser(nickname);
        assert (mainPageOfPortal.getUserNameInSearchBar().equals(nickname));
        mainPageOfPortal.goIntoUserInformation();
        assert (mainPageOfPortal.getUserNameFromPersonAccount().contains("Станислав"));
        assert (mainPageOfPortal.getUserNameFromPersonAccount().contains("Старорусов"));
        assert (mainPageOfPortal.getEmailText()).equals("starorusovsi@sovcombank.ru");
        assert (mainPageOfPortal.getPhoneText()).equals("+79271120004");
        mainPageOfPortal.logOut();
    }
}