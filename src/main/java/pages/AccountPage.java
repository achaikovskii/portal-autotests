package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountPage {
    private final WebDriver driver;

    public AccountPage(WebDriver driver){this.driver = driver;}

    public String getTitleOfPersonalAccount(){
        return driver.findElement(By.xpath("//div[@class='persona_info_content']/p/a")).getText();
    }

    public void goIntoPersonalAccount(){
        driver.findElement(By.xpath("//div[@class='persona_info_content']/p/a")).click();
    }

    public String getUserNameInSearchBar(){
        return driver.findElement(By.xpath("//a[contains(text(), 'Старорусов Станислав Игоревич')]")).getText();
    }

    public String getUserNameFromPersonAccount(){
        return driver.findElement(By.xpath("//div[@class='avatar l_float']/following::div/h3")).getText();
    }

    public void goIntoUserInformation(){
        driver.findElement(By.xpath("//a[contains(text(), 'Старорусов Станислав Игоревич')]")).click();
    }

    public String getEmailText(){
        return driver.findElement(By.xpath("//li[@class='pi5']/a")).getText();
    }

    public String getPhoneText(){
        return driver.findElement(By.xpath("//li[@class='phone']/a")).getText();
    }

    public void logOut(){
        driver.findElement(By.xpath("//li[@class='exit px-4']/img")).click();
    }

    public void checkGiveAwayCarma() {
        driver.findElement(By.xpath("//span[text()='перевод кармы']")).click();
        driver.findElement(By.xpath("//div[text()='Перевод другу']")).isDisplayed();
        driver.findElement(By.xpath("//div[@class='karma d-flex align-center']/span")).isDisplayed();
        driver.findElement(By.xpath("//div[@class='form-actions mt-2']/button[1][@disabled]"));
        driver.findElement(By.xpath("//div[@placeholder='Введите имя или выберите пользователя']//div[@class='v-select__selections']/input")).sendKeys("Старорусов");
        driver.findElement(By.xpath("//div[@class='v-list-item__content']/div[text()='Старорусов Станислав Игоревич']")).click();
        driver.findElement(By.xpath("//div[@class='v-list-item__title font-weight-medium'][contains(text(),'Старорусов')]")).isDisplayed();
        driver.findElement(By.xpath("//div[@class='v-text-field__slot']/input")).sendKeys("5");
        driver.findElement(By.xpath("//div[@class='form-actions mt-2']/button[1][not(@disabled)]"));
        driver.findElement(By.xpath("//span[text()='отменить']")).click();
    }
}
