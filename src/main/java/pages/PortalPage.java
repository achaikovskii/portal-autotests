package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class PortalPage{
    private final WebDriver driver;

    public PortalPage (WebDriver driver){ this.driver = driver; }

    public void checkTitleOfFavoriteNews() {
        ArrayList<String> nameOfNews = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            nameOfNews.add(driver.findElement(By.xpath("//div[@class='news_wrapper']//following::h2[" + i + "]/a")).getText());
            driver.findElement(By.xpath("//div[@class='news_wrapper']//following::h2[" + i + "]/a")).click();
            compareTitleNewsAndGoAway(nameOfNews, i);
        }
    }

    public void checkTitleOfNews(){
        ArrayList<String> nameOfNews = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            nameOfNews.add(driver.findElement(By.xpath("//div[@class='library_carousel grid relative']//following::div[@class='lesson-card__title'][" + i + "]/a")).getText());
            driver.findElement(By.xpath("//div[@class='library_carousel grid relative']//following::div[@class='lesson-card__title'][" + i + "]/a")).click();
            compareTitleNewsAndGoAway(nameOfNews, i);
        }
    }

    //Сравнение имеми в списке и карточке товара и выход обратно в список
    public void compareTitleNewsAndGoAway(ArrayList<String> nameOfNews, int i){
        assert (driver.findElement(By.xpath("//div[@class='item_content']/h2/span")).getText().equals(nameOfNews.get(i-1)));
        driver.findElement(By.xpath("//span[text()='Главная']")).click();
    }

    public void getContentOfPortal(){
        driver.findElement(By.xpath("//h1[text()='Новое на Портале']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()='Дни рождения коллег']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()=' Чемпионат глупостей ']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()=' Конкурсы ']")).isDisplayed();
        driver.findElement(By.xpath("//h1[text()='Обсуждения']")).isDisplayed();
        driver.findElement(By.xpath("//h1[text()='Пульс']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()='Вопрос дня']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()='Карма']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()='Опрос']")).isDisplayed();
        driver.findElement(By.xpath("//div[text()='Календарь']")).isDisplayed();
        driver.findElement(By.xpath("//h1[text()='Новые материалы']")).isDisplayed();
    }

    public void checkMenu(){
        ArrayList<String> menuItems = initialMenuItems();
        driver.findElement(By.xpath("//div[@class='menu_item button mainpage active']//span[text()='Главная']")).isDisplayed();
        for (int i=1; i<=16; i++){
            assert (driver.findElement(By.xpath("//following::div[@class='menu_overflow']/div[@class='menu_item button material']["+i+"]")).getText().equals(menuItems.get(i-1)));
        }
    }

    public ArrayList<String> initialMenuItems(){
        ArrayList<String> menuItems = new ArrayList<>();
        menuItems.add("Медиа");
        menuItems.add("Соц программы");
        menuItems.add("Фонд «Мы - Совкомбанк»");
        menuItems.add("Сообщества");
        menuItems.add("Обсуждения");
        menuItems.add("Мероприятия");
        menuItems.add("Новости");
        menuItems.add("Конкурсы");
        menuItems.add("Рейтинг");
        menuItems.add("Отзывы клиентов");
        menuItems.add("Интернет-магазин");
        menuItems.add("Чемпионат глупостей");
        menuItems.add("Пульс");
        menuItems.add("Адресная книга");
        menuItems.add("Глоссарий");
        menuItems.add("Помощь");
        return menuItems;
    }
}
