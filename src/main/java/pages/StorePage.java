package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class StorePage {
    private final WebDriver driver;

    public StorePage(WebDriver driver){
        this.driver = driver;
    }

    public void goToStore(){
        driver.findElement(By.xpath("//div[@class='menu_overflow']//span[text()='Интернет-магазин']")).click();
        driver.findElement(By.xpath("//button[@class='shop-menu__element shop-menu__switcher']")).isDisplayed();
    }

    public Integer chooseItemCheckPrice(){
        driver.findElement(By.xpath("//a[@title='Товары для дома']")).click();
        Integer priceItemOfList = Integer.valueOf(driver.findElement(By.xpath("//h1[text()='Термос для еды Takk Pappa']/following::h2[1]")).getText());
        driver.findElement(By.xpath("//h1[text()='Термос для еды Takk Pappa']")).click();
        Integer priceChosenItem = Integer.valueOf(driver.findElement(By.xpath("//div[@class='card-other__cost']/h2")).getText());
        assert (priceChosenItem.equals(priceItemOfList));
        return priceChosenItem;
    }

    public void addFirstItemToBasket(){
        driver.findElement(By.xpath("//button[@class='button button-card']")).click();
        assert (driver.findElement(By.xpath("//small[@class='basket__counter']")).getText().equals("1"));
        driver.findElement(By.xpath("//button[@class='shop-menu__element shop-menu__basket']")).click();
        driver.findElement(By.xpath("//button[@class='button large btn-buy']")).isDisplayed();
    }

    public Integer getSummaryPrice(){
        return Integer.valueOf(driver.findElement(By.xpath("//div[@class='basket-footer__summary']")).getText());
    }

    public String deleteOneItemFromBasket(){
        driver.findElement(By.xpath("//button[@class='button cross']")).click();
        return driver.findElement(By.xpath("//h1[text()='Корзина пуста']")).getText();
    }

    public Integer getNumberThisPage(){
        return Integer.valueOf(driver.findElement(By.xpath("//span[@class='active']")).getText());
    }

    public void goToPointerPage(String pointer){
        driver.findElement(By.xpath("//div[@class='pagination']//span[text()='" + pointer + "']")).click();
    }

    public void goToNumberPage(String page){
        driver.findElement(By.xpath("//a[text()='" + page + "']")).click();
    }

    public boolean checkNameItemOnPage(String nameItem){
        return driver.findElement(By.xpath("//h1[text()='" + nameItem + "']")).isDisplayed();
    }

    public void compareNameItem(){
        ArrayList<String> nameOfList = new ArrayList<>();
        for (int i=1; i<=10; i++){
            nameOfList.add(driver.findElement(By.xpath("//div[@class='shop-content__cards']//following::h1[" + i + "]")).getText());
        }
        for (int i=1; i<=10; i++){
            driver.findElement(By.xpath("//div[@class='shop-content__cards']//following::h1[" + i + "]")).click();
            assert (driver.findElement(By.xpath("//div[@class='card']//h2")).getText().equals(nameOfList.get(i-1)));
            driver.findElement(By.xpath("//span[text()='Все']")).click();
        }
    }
}
