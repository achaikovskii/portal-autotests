package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class AuthorizationPage {
    private final WebDriver driver;

    public AuthorizationPage(WebDriver driver){
        this.driver = driver;
    }

    public void inputAuthorizationForm(String login, String password) {
        driver.findElement(By.xpath("//input[@id='auth-form-email']")).sendKeys(login);
        driver.findElement(By.xpath("//input[@id='auth-form-password']")).sendKeys(password);
        driver.findElement(By.xpath("//button[text()='Войти']")).click();
    }

    public void findUser(String nickName){
        driver.findElement(By.xpath("//input[@placeholder='Поиск']")).sendKeys(nickName);
        driver.findElement(By.xpath("//input[@placeholder='Поиск']")).sendKeys(Keys.ENTER);
    }

    public void closeModalWindow(){
        driver.findElement(By.xpath("//button[@class='modal__btn']")).click();
    }
}
